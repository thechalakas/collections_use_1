﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections_Use_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //lets do some array stuff
            array_stuff_1();

            //lets do some list stuff
            list_stuff_1();

            //lets do some dictionary stuff
            dictionary_stuff_1();

            //lets do some set stuff
            set_stuff_1();

            //lets do some queue stuff
            quest_stuff_1();

            //lets do some stack stuff
            stack_stuff_1();

            //lets do some custom collection stuff
            custom_collection_stuff_1();

            Console.WriteLine("I know now why you cry but it is something that I can never do");
            Console.WriteLine("That's all folks!");

            Console.ReadLine();

        }

        //this method will demonstrate custom collections
        private static void custom_collection_stuff_1()
        {
            //creating an instance of the water bottle custom collection
            Water_Bottle_CustomCollection temp_water_bottle_collection = new Water_Bottle_CustomCollection();

            //creating some water bottles
            Bottle_Water bottle_1 = new Bottle_Water { serial_no = 1, name = "bottle 1" };
            Bottle_Water bottle_2 = new Bottle_Water { serial_no = 2, name = "bottle 2" };
            Bottle_Water bottle_3 = new Bottle_Water { serial_no = 3, name = "bottle 3" };
            Bottle_Water bottle_4 = new Bottle_Water { serial_no = 4, name = "bottle 4" };
            Bottle_Water bottle_5 = new Bottle_Water { serial_no = 5, name = "bottle 5" };
            Bottle_Water bottle_6 = new Bottle_Water { serial_no = 6, name = "bottle 6" };

            //adding these bottles to the collection 
            temp_water_bottle_collection.Add(bottle_1);
            temp_water_bottle_collection.Add(bottle_2);
            temp_water_bottle_collection.Add(bottle_3);
            temp_water_bottle_collection.Add(bottle_4);
            temp_water_bottle_collection.Add(bottle_5);
            temp_water_bottle_collection.Add(bottle_6);

            //displaying the collection
            Console.WriteLine("displaying the water bottles");
            foreach(Bottle_Water temp in temp_water_bottle_collection)
            {
                Console.WriteLine(" bottle serial no {0} bottle name {1}", temp.serial_no, temp.name);
            }

            //removing some bottles based on serial number
            temp_water_bottle_collection.remove_water_bottle(2);
            temp_water_bottle_collection.remove_water_bottle(4);

            //displaying the collection after removing some bottles
            Console.WriteLine("displaying the water bottles after removing some bottles");
            foreach (Bottle_Water temp in temp_water_bottle_collection)
            {
                Console.WriteLine(" bottle serial no {0} bottle name {1}", temp.serial_no, temp.name);
            }
        }


        //this method will demonstrate usage of a stack
        private static void stack_stuff_1()
        {
            Stack<int> temp_stack = new Stack<int>();

            //pushing some elements into the stack
            //since this is a stack, newly added elements end up at the top
            temp_stack.Push(0);
            temp_stack.Push(1);
            temp_stack.Push(2);
            temp_stack.Push(3);
            temp_stack.Push(4);

            //displaying the stack
            Console.WriteLine("showing the stack");
            foreach(int x in temp_stack)
            {
                Console.WriteLine("{0}",x);
            }

            //popping some elements from the stack
            //in a stack, the most recently added elements end up being removed
            Console.WriteLine("{0} has been popped out", temp_stack.Pop());
            Console.WriteLine("{0} has been popped out", temp_stack.Pop());
            Console.WriteLine("{0} has been popped out", temp_stack.Pop());

            //displaying the stack again.
            //displaying the stack
            Console.WriteLine("showing the stack after the popping");
            foreach (int x in temp_stack)
            {
                Console.WriteLine("{0}", x);
            }
        }

        //this method will demonstrate usage of queue in c sharp
        private static void quest_stuff_1()
        {
            //creating a queue
            Queue<int> temp_queue = new Queue<int>();

            //adding some elements
            temp_queue.Enqueue(0);
            temp_queue.Enqueue(1);
            temp_queue.Enqueue(2);
            temp_queue.Enqueue(3);
            temp_queue.Enqueue(4);
            temp_queue.Enqueue(5);

            Console.WriteLine("showing the queue from the initial set");
            foreach(int x in temp_queue)
            {
                Console.WriteLine("{0}", x);
            }
            //removing some elements, in this case, queue is first come first leave
            Console.WriteLine("{0} has been removed from the queue", temp_queue.Dequeue());
            Console.WriteLine("{0} has been removed from the queue", temp_queue.Dequeue());
            Console.WriteLine("{0} has been removed from the queue", temp_queue.Dequeue());


            //showing the queue
            Console.WriteLine("showing the queue after removing some elements");
            foreach(int x in temp_queue)
            {
                Console.WriteLine("{0}", x);
            }

            //adding some more elements
            temp_queue.Enqueue(6);
            temp_queue.Enqueue(7);
            temp_queue.Enqueue(8);
            temp_queue.Enqueue(9);
            temp_queue.Enqueue(10);
            //showing the queue
            Console.WriteLine("showing the queue after after adding some elements");
            foreach (int x in temp_queue)
            {
                Console.WriteLine("{0}", x);
            }
        }


        //this method will do some set stuff
        private static void set_stuff_1()
        {
            //creating one set with some elements
            HashSet<int> set_one = new HashSet<int>();
            set_one.Add(1);
            set_one.Add(3);
            set_one.Add(5);
            set_one.Add(7);
            set_one.Add(9);

            //creating another set with some other elements
            HashSet<int> set_two = new HashSet<int>();
            set_two.Add(0);
            set_two.Add(2);
            set_two.Add(4);
            set_two.Add(6);
            set_two.Add(8);
            set_two.Add(0);

            //creating a new set that combines (union of set) the above two sets
            HashSet<int> set_three = new HashSet<int>();
            set_three.UnionWith(set_one);
            set_three.UnionWith(set_two);

            //displaying the three sets

            Console.WriteLine("display the first set");
            foreach(int x in set_one)
            {
                Console.WriteLine("{0}", x);
            }
            Console.WriteLine("display the second set");
            foreach (int x in set_two)
            {
                Console.WriteLine("{0}", x);
            }
            Console.WriteLine("displaying the set which is an union of the above sets");
            foreach (int x in set_three)
            {
                Console.WriteLine("{0}", x);
            }
        }

        //this will do some dictionary stuff
        private static void dictionary_stuff_1()
        {
            var temp_dictionary_list_1 = return_simple_dictionary();

            //lets display the dictionary list
            Console.WriteLine("displaying the dictionary list");
            foreach(var temp in temp_dictionary_list_1)
            {
                Console.WriteLine("the key is {0} value is {1}", temp.Key, temp.Value);
            }

            //lets add an element
            temp_dictionary_list_1.Add(5, "oh yeah baby");
            //lets remove an element
            temp_dictionary_list_1.Remove(1);

            Console.WriteLine("displaying the dictionary list after making some changes");
            foreach (var temp in temp_dictionary_list_1)
            {
                Console.WriteLine("the key is {0} value is {1}", temp.Key, temp.Value);
            }
        }

        //this will do some list stuff
        private static void list_stuff_1()
        {
            //getting the list first
            var list_temp_1 = return_simple_string_list();

            //lets display the list
            foreach(string s in list_temp_1)
            {
                Console.WriteLine("the string is {0}", s);
            }

            //now adding some elements to the list
            list_temp_1.Add("ola amigo");
            list_temp_1.Add("astala vista baby");

            Console.WriteLine("displaying the list after adding two elements");
            //displaying
            foreach (string s in list_temp_1)
            {
                Console.WriteLine("the string is {0}", s);
            }

            //removing some stuff
            list_temp_1.Remove("wassup");

            Console.WriteLine("displaying the list after removing one element");
            //displaying again
            foreach (string s in list_temp_1)
            {
                Console.WriteLine("the string is {0}", s);
            }
        }

        //this will do some array stuff
        private static void array_stuff_1()
        {
            //getting the two arrays
            var temp_array_1 = return_simple_array_1();
            var temp_array_2 = return_2_dimenstional_array();

            //now running through them and displaying them
            Console.WriteLine("displaying the single dimenstional array now");
            foreach(int i in temp_array_1)
            {
                Console.WriteLine(" element is {0}", i);
            }

            Console.WriteLine("displaying the two dimenstional array now");
            for (int i=0;i<temp_array_2.Length;i++)
            {
                
            }
        }

        //this function will return a simply array
        static int[] return_simple_array_1()
        {
            int[] simple_array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

            //returning the array
            return (simple_array);
        }

        //this will return a 2 dimenstional array
        static int[,] return_2_dimenstional_array()
        {
            //a 2 dimenstional array with 3 rows and each row has 12 elements.
            int[,] dimentional_array_two = new int[3, 12]
            {
                {1,2,3,4,5,6,7,8,9,10,11,12 },
                {10,20,30,40,50,60,70,80,90,100,110,120 },
                {100,200,300,400,500,600,700,800,900,1000,1100,1200 }

            };

            return (dimentional_array_two);
        }


        //this will return a simple list
        static List<string> return_simple_string_list()
        {
            List<string> temp_list_strings_1 = new List<string>
            {
                "wassup",
                "howz it going",
                "where are thou",
                "hip to the ham brother"
            };

            return (temp_list_strings_1);
        }

        //this will return a simple dictionary 
        static Dictionary<int,string> return_simple_dictionary()
        {
            Dictionary<int, string> temp_dictionary_1 = new Dictionary<int, string>();

            //adding some elements to the dictionary
            //dictionary needs a key and also a value
            temp_dictionary_1.Add(1, "wazz going on");
            temp_dictionary_1.Add(2, "who are you");
            temp_dictionary_1.Add(3, "who let the dogs out");

            return (temp_dictionary_1);
        }
    }

    class Water_Bottle_CustomCollection : List<Bottle_Water>
    {
        //this will remove 
        public void remove_water_bottle(int serial_number_to_remove)
        {
            //we will loop through the collection, find the bottle with the serial number
            //starting from the last element in the list, and then going down till we hit the very first item
            for(int i = this.Count - 1;i>=0;i--)
            {
                //compare the current water bottle with the serial number provided
                if(this[i].serial_no == serial_number_to_remove)
                {
                    Bottle_Water temp = this[i];
                    
                    //if we reached here, that means we found the bottle to remove
                    this.RemoveAt(i);

                    //displaying what was removed
                    Console.WriteLine(" water bottle {0} with serial number {1} removed", temp.name, temp.serial_no);
                }
            }
        }
    }

    internal class Bottle_Water
    {
        public int serial_no;
        public string name;
    }
}
